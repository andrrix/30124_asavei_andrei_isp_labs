package isp.lab3.exercise4;

import junit.framework.TestCase;
import org.junit.Test;

import static java.lang.String.valueOf;

public class MyPointTest extends TestCase {
    MyPoint point1=new MyPoint(1, 14,30);
    MyPoint point2=new MyPoint(48, 56, 23);
    @Test
    public void testDistanceBetweenAPointAndAnother() {
        assertEquals("Should that distance between point1 and another point with x,y,z is: ",
                "14.21267", valueOf(point1.distance(1,5,19)));
    }
    @Test
    public void testDistanceBetweenPoint1AndPoint2() {
        assertEquals("Should that distance between point1 and point2 is: ",
                "63.41924", valueOf(point1.distance(point2)));
    }

}