package isp.lab3.exercise3;

import junit.framework.TestCase;
import org.junit.Test;

public class VehicleTest extends TestCase {
    Vehicle v1=new Vehicle("Volswagen", "Passat", 160, 'D');
    @Test
    public void testModel() {
        assertEquals("Should have 'Volswagen (Passat) speed 160 fuelType D'",
                "Volswagen (Passat) speed 160 fuelType D", v1.toString());
    }

}