package isp.lab3.exercise1;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TreeTest extends TestCase {
    Tree tree1 = new Tree();

    @Test
    public void testGrow() {
        tree1.grow(15);
        assertEquals("Should add '10' at height", "30", tree1.toString());
    }

    @Test
    public void testToString() {
        assertEquals("Should print height", "15", tree1.toString());
    }
}