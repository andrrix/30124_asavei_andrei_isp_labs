package isp.lab3.exercise3;

public class Exercise3 {
    public static void main(String[] args) {
        Vehicle v1=new Vehicle("Volswagen", "Passat", 160, 'D');
        Vehicle v2=new Vehicle("Audi", "B7", 120, 'B');
        Vehicle v3=new Vehicle("Volswagen", "Passat", 160, 'D');
        System.out.println(v1);
        v1.setFuelType('D');
        v1.setModel("Mercedes");
        v1.setType("C CLass");
        v1.setSpeed(300);
        System.out.println("The " + v1.getModel() + " "+ v1.getType() +" have speed "+ v1.getSpeed()
                    +", the fuel is "+ v1.getFuelType() );
        v2.setFuelType('D');
        v2.setModel("Nissan");
        v2.setType("Patrol");
        v2.setSpeed(250);
        System.out.println("The " + v2.getModel() + " "+ v2.getType() +" have speed "+ v2.getSpeed()
                +", the fuel is "+ v2.getFuelType() );
        v3.setFuelType('D');
        v3.setModel("Mercedes");
        v3.setType("C CLass");
        v3.setSpeed(300);
        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v1.equals(v3));

    }
}
