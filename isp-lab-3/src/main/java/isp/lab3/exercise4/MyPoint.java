package isp.lab3.exercise4;

public class MyPoint {
   private int x;
   private int y;
   private int z;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public MyPoint(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }
    public void setXYZ(int x, int y, int z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    @Override
    public String toString() {
        return "("+x+","+y+","+z;
    }
    public float distance(int x, int y, int z){
        float difX= (float) this.getX()-x;
        float difY=(float) this.getY()-y;
        float difZ= (float) this.getZ()-z;
       return (float) Math.sqrt(Math.pow(difX,2)+Math.pow(difY,2)+Math.pow(difZ,2));
    }
    public float distance(MyPoint another)
    {float difX= (float) this.getX()-another.getX();
        float difY=(float) this.getY()-another.getY();
        float difZ= (float) this.getZ()-another.getZ();
        return (float) Math.sqrt(Math.pow(difX,2)+Math.pow(difY,2)+Math.pow(difZ,2));
    }

}
