package isp.lab3.exercise4;

public class Exercise4 {
    public static void main(String[] args) {
        MyPoint point1=new MyPoint(1, 14,30);
        MyPoint point2=new MyPoint(48, 56, 23);
        System.out.println("The distance between point1 and another point with x,y,z is: " + point1.distance(1,5,19));
        System.out.println("The distance between point1 and point2 is: " + point1.distance(point2));
    }
}
