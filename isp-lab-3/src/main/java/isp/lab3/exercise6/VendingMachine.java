package isp.lab3.exercise6;

import java.util.Scanner;

public class VendingMachine {
    private int machineCredit;
    private Product[] products1;

    private static final VendingMachine instance=new VendingMachine();
    private VendingMachine() {
        this.products1 = new Product[5];
        this.products1[0] = new Product("produs1", 1);
        this.products1[1] = new Product("produs2", 2);
        this.products1[2] = new Product("produs2", 3);
        this.products1[3] = new Product("produs4", 4);
        this.products1[4] = new Product("produs5", 5);
        this.machineCredit = 0;
    }
    public static VendingMachine getInstance(){
        return instance;
    }



    public void setMachineCredit(int machineCredit) {
        this.machineCredit = machineCredit;
    }

    public void setProducts(Product[] products) {
        this.products1 = products;
    }

    public int getMachineCredit() {
        return machineCredit;
    }

    public Product[] getProducts() {
        return products1;
    }

    public void displayProducts() {
        for (Product product : this.getProducts())
            System.out.println(product.getName() + " : " + product.getId());
    }

    public void insertCoin(int value) {
        this.machineCredit += value;
    }

    public String selectProduct(int id) {
        for (Product product : this.getProducts())
            if (product.getId() == id)
                return product.getName();

            String s="error at the search of a id product";
                return s;
                
    }

    public void displayCredit() {
        System.out.println("The available credit is: " + this.getMachineCredit());
    }

    public void userMenu()
    {
        Scanner s=new Scanner(System.in);
        //String Opt = null ;
       for(;;) {
           System.out.println("VendingMachine operations - Chose one option\n1. Create an VendingMachine Opbject" +
                   "\n2.Set Credit for VendingMachine\n3.Display products for VendingMachine\n" +
                   "4.Add coins to current vending machine credit\n5.Select a product\n6.Display Credit for VendingMachine\n7.Exit");

           int opt1 = s.nextInt();
           Scanner s1 = new Scanner(System.in);
           switch (opt1) {
               case 1: {
                   System.out.println("You chose to create an VendingMachine Object");
                   System.out.println("Give one by step 5 products and their indexes:");
                   Product[] products1 = new Product[5];
                   System.out.println("Name of product 1: ");
                   products1[0].setName(s1.next());
                   System.out.println("Index of product 1: ");
                   products1[0].setId(Integer.parseInt(s1.next()));
                   System.out.println("Name of product 2: ");
                   products1[1].setName(s1.next());
                   System.out.println("Index of product 2: ");
                   products1[1].setId(Integer.parseInt(s1.next()));
                   System.out.println("Name of product 3: ");
                   products1[2].setName(s1.next());
                   System.out.println("Index of product 3: ");
                   products1[2].setId(Integer.parseInt(s1.next()));
                   System.out.println("Name of product 4: ");
                   products1[3].setName(s1.next());
                   System.out.println("Index of product 4: ");
                   products1[3].setId(Integer.parseInt(s1.next()));
                   System.out.println("Name of product 5: ");
                   products1[4].setName(s1.next());
                   System.out.println("Index of product 5: ");
                   products1[4].setId(Integer.parseInt(s1.next()));
                   this.setProducts(products1);
                   System.out.println("Give the initial machineCredit:");
                   this.setMachineCredit(s1.nextInt());
                   System.out.println("It's done, you created an VendingMachine");
                   break;
               }
               case 2:
                   System.out.println("Set Credit for VendingMachine; Give a credit");
                   this.setMachineCredit(s1.nextInt());
                   break;
               case 3:
                   System.out.println("Display products for VendingMachine");
                   this.displayProducts();
                   break;
               case 4:
                   System.out.println("Add coins to current vending machine credit; give coins");
                   this.insertCoin(s1.nextInt());
                   break;
               case 5:
                   System.out.println("Select a product; Give id");
                   System.out.println(this.selectProduct(s1.nextInt()));
                   break;
               case 6:
                   System.out.println("Display Credit for VendingMachine");
                   this.displayCredit();
                   break;
               case 7: return;

           }
       }
    }
}
