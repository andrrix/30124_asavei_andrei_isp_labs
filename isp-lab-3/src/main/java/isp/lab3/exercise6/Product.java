package isp.lab3.exercise6;

public class Product {
    private String name;
    private int id;

    public Product() {
    }

    public Product(String name, int id) {
        this.name=name;
    this.id=id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
