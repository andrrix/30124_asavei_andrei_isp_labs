package isp.lab3.exercise2;

public class Exercise2 {
    public static void main(String[] args) {
        Rectangle rectangle=new Rectangle(3, 5);
        System.out.println("The length have: " + rectangle.getLength() + " cm");
        System.out.println("The width have: " + rectangle.getWidth() + " cm");
        System.out.println("The color is: " + rectangle.getColor());
        System.out.println("The perimeter have: " + rectangle.getPerimeter() + " cm");
        System.out.println("The area have: " + rectangle.getArea() + " cm^2");

    }
}
