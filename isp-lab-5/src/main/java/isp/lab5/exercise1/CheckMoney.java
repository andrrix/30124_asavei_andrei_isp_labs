package isp.lab5.exercise1;

public class CheckMoney extends Transaction {

    public CheckMoney(Account account) {
        super(account);
    }

    public String execute(){
        if (this.account == null) {
            return "Don't exist an account";
        }
        return String.format("Your balance is: %f", this.account.getBalance());

    }
}
