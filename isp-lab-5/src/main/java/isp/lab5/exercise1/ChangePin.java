package isp.lab5.exercise1;

import java.util.Scanner;

public class ChangePin extends Transaction {
    private String oldPin;
    private String newPin;

    public ChangePin(String oldPin,String newPin, Account account) {
        super(account);
        this.oldPin = oldPin;
        this.newPin=newPin;


    }

    public void setOldPin(String oldPin) {
        this.oldPin = oldPin;
    }

    public void setNewPin(String newPin) {
        this.newPin = newPin;
    }

    public String getOldPin() {
        return this.oldPin;
    }

    public String getNewPin() {
        return this.newPin;
    }

    public String execute() {
        if (this.account == null) {
            return "Don't exist an account";
        } else {
//            Scanner scanner = new Scanner(System.in);
//            String pin;
//            System.out.print("For change pin, ");
//            for (int i = 1; i <= 3; i++) {
//                System.out.println("give the old pin (4 digit):");
//                pin = scanner.nextLine();
//                if (pin.equals(this.getOldPin())) {
                   this.setNewPin(this.newPin);
                   account.getCard().setPin(this.newPin);
                    return String.format("Your pin is %s", this.getNewPin());
//                }
//                else System.out.printf("Incorrect pin, you have %d more attempts%n", 3 - i);
//
//            }
//            return String.format("Your attempts is done, you couldn't change your pin ", this.getNewPin());


        }
    }
}
