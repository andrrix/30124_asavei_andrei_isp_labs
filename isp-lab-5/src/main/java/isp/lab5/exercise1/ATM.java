package isp.lab5.exercise1;

import java.sql.SQLOutput;

public class ATM {
    Bank bank;
    Card card=null;

    public ATM(Bank bank) {
        this.bank = bank;
    }
    public boolean insertCard(Card card, String pin){
        if(card.getPin().equals(pin)) {
            this.card = card;
            return true;
        }
        return false;
    }
    public void removeCard(){
        this.card=null;
    }

    public void changePin(String oldPin, String newPin){
        ChangePin changePin=new ChangePin(oldPin, newPin, this.bank.getAccountByCard(card.getCardId()));
        System.out.println(changePin.execute());
    }
    public void withdraw(double amount){
         Account a=bank.getAccountByCard(card.getCardId());
        WithdrawMoney withdrawMoney=new WithdrawMoney(amount,a);
        System.out.println(withdrawMoney.execute());
    }
    public void checkMoney(){
        CheckMoney checkMoney=new CheckMoney(this.bank.getAccountByCard(card.getCardId()));
        System.out.println(checkMoney.execute());

    }
}
