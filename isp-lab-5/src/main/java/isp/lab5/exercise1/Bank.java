package isp.lab5.exercise1;

public class Bank {
    Account[] accounts=new Account[10];

    public Bank(Account[] accounts) {
        this.accounts = accounts;
    }

    public String executeTransaction( Transaction t){
        return t.execute();
    }
    
    public Account getAccountByCard( String cardId){
        for(Account account : this.accounts)
            if(account.getCard().getCardId().equals(cardId))
            return account;
        System.out.println("Doesn't exist account with the cardID :"+cardId);
            return null;
    }
}
