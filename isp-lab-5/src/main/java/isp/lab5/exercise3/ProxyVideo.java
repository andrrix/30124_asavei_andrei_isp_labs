package isp.lab5.exercise3;


public class ProxyVideo implements Playable {
    private ColorVideo colorVideo;
    private BlackAndWhiteVideo blackAndWhiteVideo;
    private String fileName;
    private String typeVideo;

    public ProxyVideo(String fileName, String typeVideo) {

        this.fileName = fileName;
        this.typeVideo = typeVideo;
    }

    @Override
    public void play() {
        if (this.typeVideo.equals("Black and white video")) {
            if (this.blackAndWhiteVideo == null) {
                this.blackAndWhiteVideo = new BlackAndWhiteVideo(fileName);
            }
            this.blackAndWhiteVideo.play();
                }
        if (this.typeVideo.equals("Color video")) {
            if (this.colorVideo == null) {
                this.colorVideo = new ColorVideo(fileName);
            }
            this.colorVideo.play();
        }

    }
}
