package isp.lab5.exercise4;

public class Rectangle implements Shape {
    public Rectangle() {
    }

    @Override
    public void draw() {
        System.out.println("A rectangle is drawn");
    }
}
