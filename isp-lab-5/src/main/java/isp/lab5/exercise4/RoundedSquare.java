package isp.lab5.exercise4;

public class RoundedSquare implements Shape{
    public RoundedSquare() {
    }

    @Override
    public void draw() {
        System.out.println("An rounded square is drawn");
    }
}
