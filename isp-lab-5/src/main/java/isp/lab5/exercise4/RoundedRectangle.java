package isp.lab5.exercise4;

public class RoundedRectangle implements Shape {
    
    public RoundedRectangle() {
    }

    @Override
    public void draw() {
        System.out.println("A rounded rectangle is drawn");
    }
}
