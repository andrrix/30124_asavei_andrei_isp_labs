package isp.lab5.exercise1;

import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Exercise1Test extends TestCase {
    Card card1 = new Card("896701", "1234");
    Card card2 = new Card("474829", "4389");
    ATM atm = new ATM(new Bank(new Account[]
            {new Account("Andrei", 2000, card1),
                    new Account("Dan", 100, card2)}));

    @Test
    public void testInsertCard() {
        assertEquals("It should be corect",true, atm.insertCard(card1, "1234"));
        assertEquals("It should be false",false ,atm.insertCard(card2,"1456"));
    }

    @Test
    public void testRemoveCard() {
        atm.insertCard(card1,"1234");
        atm.removeCard();
        assertEquals("card it should be null",null,atm.card );

    }

    @Test
    public void testChangePin() {
        atm.insertCard(card1,"1234");
        atm.changePin("1234", "4567");
         assertEquals("A pin should be 4567", "4567",String.valueOf(card1.getPin()) );
    }
    @Test
    public void testWithdrawMoney() {
        atm.insertCard(card1,"1234");
        atm.withdraw(100);
        assertEquals("it should be 1900 ", 1900, atm.bank.getAccountByCard("896701").getBalance(),1);

    }
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeClass
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }
    @Test
    public void testCheckMoney() {
        atm.insertCard(card2, "4389");
        atm.checkMoney();
        assertEquals("It should be 100","Your balance is: 100.000000", outputStreamCaptor.toString()
                .trim());

    }




}