package isp.lab1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author mihai.hulea
 */
public class MathBasicOperationsTest {

    @Test
    public void testAdd(){
        assertEquals("Should add 2 numbers", 30, MathBasicOperations.add(10,20));
    }

    @Test
    public void testSubstract(){
        assertEquals("Should substract 2 numbers", 5, MathBasicOperations.substract(15,10));
    }

    @Test
    public void testMultiplication(){assertEquals("Should multiply 2 numbers",120, MathBasicOperations.multiplication(20,6) );}
    @Test
    public void testDivide(){assertEquals("Just i'm witting smthng bla bla, but i should divide 2 numbers",
            11,  MathBasicOperations.divide(110, 10));}
}
