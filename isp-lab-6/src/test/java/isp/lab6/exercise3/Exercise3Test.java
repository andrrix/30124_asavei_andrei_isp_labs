package isp.lab6.exercise3;

import junit.framework.TestCase;
import org.junit.Test;

public class Exercise3Test extends TestCase {
    SensorCluster c = new SensorCluster();

    @Test
    public void testaddSensor(){
        c.addSensor("1",SensorType.TEMP);
        assertEquals("should have 1 added object", 1, c.sensors.size() );
    }
    @Test
    public void testWriteSensorReading(){
        c.addSensor("1",SensorType.TEMP);
        c.writeSensorReading("1",8,9);
        assertEquals("should have value:8", 8.0, c.sensors.get(0).readings.get(0).getValue() );
        assertEquals("should have date time:9", 9, c.sensors.get(0).readings.get(0).getDateAndTime() );
    }
    @Test
    public void testGetSensorById(){
        c.addSensor("1",SensorType.TEMP);
        Sensor s=c.sensors.get(0);
        c.addSensor("2",SensorType.TEMP);

        assertEquals("should have s", s, c.getSensorById("1"));
    }


}