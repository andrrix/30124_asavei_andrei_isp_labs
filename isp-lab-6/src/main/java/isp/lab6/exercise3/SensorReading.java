package isp.lab6.exercise3;

import java.util.Comparator;

/////////////////////////////////////////
class SensorReading implements Comparable<SensorReading> {
    double value;
    long dateAndTime;

    public SensorReading(double value, long dateAndTime) {
        this.value = value;
        this.dateAndTime = dateAndTime;
    }
    public double getValue() {
        return value;
    }

    public long getDateAndTime() {
        return dateAndTime;
    }

//    @Override
//    public int compareTo(SensorReading o) {
//        return (int) (this.value - o.value);
//    }
    @Override
    public int compareTo(SensorReading o) {
        return (int) (this.dateAndTime - o.dateAndTime);
    }

//    //@Override
//    public int compareTo1(SensorReading o) {
//        return (int) (dateAndTime - o.dateAndTime);
//    }



}
