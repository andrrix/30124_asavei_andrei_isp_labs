package isp.lab6.exercise3;

import java.util.*;

/////////////////////////////////////////
class   Sensor implements Comparator<SensorReading> {
    ArrayList<SensorReading> readings = new ArrayList<>();
    String id;
    SensorType type;

    public Sensor() {
    }

    public Sensor(String id, SensorType type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public SensorType getType() {
        return type;
    }

    public ArrayList<SensorReading> getReadings() {
        return readings;
    }

    public boolean addSensorReading(SensorReading reading) {
        readings.add(reading);
        return true;
    }
    public int compare(SensorReading sensorReading1, SensorReading sensorReading2) {

        return Double.compare(sensorReading1.getValue(), sensorReading2.getValue());
    }

    List<SensorReading> getSensorReadingSortedByValue() {
        Comparator<SensorReading> sensorReadingComparator=Comparator.comparing(SensorReading:: getValue);
        Collections.sort(readings, sensorReadingComparator); //aceasta metoda utilizeaaz criteriul definiti in compareTo din clasa SensorReading)
        return readings;
    }


    List<SensorReading> getSensorReadingSortedByDateAndTime() {
        Collections.sort(readings); //aceasta metoda utilizeaaz criteriul definiti in compareTo din clasa SensorReading)
        return readings;

    }


}
