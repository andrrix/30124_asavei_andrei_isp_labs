package isp.lab6.exercise3;

import java.util.ArrayList;

/////////////////////////////////////////
class SensorCluster {
    ArrayList<Sensor> sensors = new ArrayList<>();

    public Sensor addSensor(String id, SensorType type) {
        for(Sensor s:sensors)
        if(id.equals(s.getId()))
            return null;
        sensors.add(new Sensor(id, type));
        return sensors.get(sensors.size() - 1);
    }

    public boolean writeSensorReading(String id, double value, long dtaeTime) {
        boolean bool=false;
        for (Sensor s : sensors) {
            if (s.id.equals(id)) {
                s.addSensorReading(new SensorReading(value, dtaeTime));
                bool=true;
            }
        }
        return bool;
    }

    public Sensor getSensorById(String id) {
        return sensors.stream().filter(s -> s.id == id).findFirst().get();
    }

}
