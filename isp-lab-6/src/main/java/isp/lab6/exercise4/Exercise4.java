package isp.lab6.exercise4;

import java.util.ArrayList;
import java.util.List;

public class Exercise4 {

    public static void main(String[] args) {
        Card card1 = new Card("896701","1234");
        Card card2=new Card("474829", "4389");
        List <Account> a1= new ArrayList<>();
        a1.add(new Account("Andrei",20000, card1));
        a1.add(new Account("Dan", 100, card2));

        Bank bank = new Bank(a1);


        ATM atm = new ATM(bank);

        atm.insertCard(card1,"1234");
        atm.withdraw(900);
        atm.checkMoney();
        System.out.println(card1.getPin());
        atm.changePin("1234", "4321");
        System.out.println(card1.getPin());


    }
}

