package isp.lab6.exercise4;

public abstract class Transaction {
    Account account;

    public Transaction(Account account) {
        this.account=account;
    }

    public abstract String  execute();


}
