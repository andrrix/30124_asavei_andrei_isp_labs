package isp.lab6.exercise4;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    List <Account> accounts=new ArrayList<>();
    //Account[] accounts=new Account[10];

    public Bank(List <Account> accounts) {
        this.accounts = accounts;
    }

    public String executeTransaction( Transaction t){
        return t.execute();
    }
    
    public Account getAccountByCard( String cardId){
        for(Account account : this.accounts)
            if(account.getCard().getCardId().equals(cardId))
            return account;
        System.out.println("Doesn't exist account with the cardID :"+cardId);
            return null;
    }
}
