package isp.lab6.exercise4;

public class WithdrawMoney extends Transaction{
    private double amount;

    public WithdrawMoney(double amount, Account account) {
        super(account);
        this.amount = amount;

    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String execute(){
        if(this.getAmount()<=this.account.getBalance())
        {   this.account.setBalance(this.account.getBalance()-this.getAmount());
            return String.format("You just withdrew the amount %f, current account balance is %f",this.getAmount(),this.account.getBalance());
        }
        else return "Insufficient funds";
    }

}
