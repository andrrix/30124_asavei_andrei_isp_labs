package isp.lab6.exercise1;

import java.util.ArrayList;
import java.util.List;

public class Exercise1 {
    public static void main(String[] args) {

        System.out.println("Test implementation here.");
        SensorReading sensorReading1=new SensorReading(20, "Cluj-Napoca", Type.TEMPERATURE);
        List <SensorReading> readingList=new ArrayList<>();
        readingList.add(sensorReading1);
        SensorReadingList sensorReadingList=new SensorReadingList(readingList);
        sensorReadingList.addReading( new SensorReading(30, "Bucuresti", Type.HUMIDITY));
        sensorReadingList.addReading(new SensorReading(2,"Dej", Type.PRESSURE));
        sensorReadingList.addReading( new SensorReading(40, "Bucuresti", Type.HUMIDITY));
        sensorReadingList.addReading( new SensorReading(8, "Constanta", Type.PRESSURE));
        sensorReadingList.addReading( new SensorReading(25, "Cluj-Napoca", Type.HUMIDITY));

        System.out.println("Average value by type "+ sensorReadingList.getAvarageValueByType(Type.HUMIDITY, "Cluj-Napoca"));
        System.out.println(sensorReadingList.getReadingsByType(Type.HUMIDITY));
        System.out.println("Sorted list by location:\n"+sensorReadingList.listSortedByLocation());
        System.out.println("Sorted list by value:\n"+sensorReadingList.listSortedByValue());
        System.out.println("List of all sensors by Location {Bucuresti} and Type{HUMIDITY} "
                +sensorReadingList.findAllByLocationAndType("Bucuresti",Type.HUMIDITY));

    }
}
