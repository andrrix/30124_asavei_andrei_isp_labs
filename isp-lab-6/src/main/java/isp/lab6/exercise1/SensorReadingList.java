package isp.lab6.exercise1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SensorReadingList implements IReadingRepository {
    private List<SensorReading> list;

    public SensorReadingList(List<SensorReading> list) {
        this.list = list;
    }

    public void setList(List<SensorReading> list) {
        this.list = list;
    }

    public List<SensorReading> getList() {
        return this.list;
    }

    public void addReading(SensorReading reading) {
        this.list.add(reading);
    }

    public double getAvarageValueByType(Type type, String location) {
        double sum = 0;
        double nr = 0;
        for (SensorReading sensorReading : this.list) {
            if (sensorReading.getType().equals(type)
                    && sensorReading.getLocation().equals(location)) {
                sum += sensorReading.getValue();
                nr++;
            }

        }
        return (double) sum / nr;
    }

    public List<SensorReading> getReadingsByType(Type type) {
        List<SensorReading> lista1 = new ArrayList<>();
        for (SensorReading sensorReading : this.list) {
            if (sensorReading.getType().equals(type))
                lista1.add(sensorReading);
        }
        return lista1;
    }

    public List<SensorReading> listSortedByLocation() {
        List<SensorReading> sortedSenzorReading = list.stream()
                .sorted(Comparator.comparing(SensorReading:: getLocation))
                .collect(Collectors.toList());
        list.clear();
        this.list.addAll(sortedSenzorReading);
        return list;

    }

    public List<SensorReading> listSortedByValue() {
        List<SensorReading> sortedSenzorReading = list.stream()
                .sorted(Comparator.comparing(SensorReading:: getValue))
                .collect(Collectors.toList());
        list.clear();
        this.list.addAll(sortedSenzorReading);
        return list;

    }


    public List<SensorReading> findAllByLocationAndType(String location, Type type) {
        List<SensorReading> list2 = new ArrayList<>();
        for (SensorReading sensorReading : this.list) {
            if (sensorReading.getType().equals(type)
                    && sensorReading.getLocation().equals(location))
                list2.add(sensorReading);
        }
        return list2;
    }


}
