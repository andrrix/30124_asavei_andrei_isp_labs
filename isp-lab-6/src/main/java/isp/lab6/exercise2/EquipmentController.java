package isp.lab6.exercise2;

import isp.lab6.exercise1.SensorReading;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class EquipmentController {
    private List<Equipment> list=new ArrayList<>();

    public EquipmentController() {

    }

    /**
     * Add new equipment to the list of equipments
     *
     * @param equipment - equipment to be added
     */
    public void addEquipment(final Equipment equipment) {
        list.add(equipment);
    }

    /**
     * Get current list of equipments
     *
     * @return list of equipments
     */
    public List<Equipment> getEquipments() {
        return this.list;
    }

    /**
     * Get number of equipments
     *
     * @return number of equipments
     */
    public int getNumberOfEquipments() {
        return list.size();
    }

    /**
     * Group equipments by owner
     *
     * @return a dictionary where the key is the owner and value is represented by list of equipments he owns
     */
    public Map<String, List<Equipment>> getEquipmentsGroupedByOwner() {
        //throw new UnsupportedOperationException("Not supported yet.");
        Map<String, List<Equipment>> equipmentListGrouped = list.stream()
                .collect(groupingBy(Equipment::getOwner));
        return equipmentListGrouped;
    }

    /**
     * Remove a particular equipment from equipments list by serial number
     * @param serialNumber - unique serial number
     * @return deleted equipment instance or null if not found
     */
    public Equipment removeEquipmentBySerialNumber(final String serialNumber) {
        //throw new UnsupportedOperationException("Not supported yet.");
        Equipment equipment=null;
        try {  Iterator<Equipment> iterator=list.iterator();
                while(iterator.hasNext()){
                    Equipment equipment1=iterator.next();
                    if(serialNumber.equals(equipment1.getSerialNumber())){
                        equipment=equipment1;
                        list.remove(equipment1);
                        break;
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return equipment;

    }
}
