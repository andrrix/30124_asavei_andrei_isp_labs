package isp.lab4.exercise1;

import junit.framework.TestCase;import org.junit.Test;
import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;

public class TemperatureSensorTest {
    //implement minimal tests
    TemperatureSensor t1=new TemperatureSensor(23,"Bedroom");

    @Test
    public void testGetValueOfTemperature() {
        assertEquals("Should have value of temperature", "23", valueOf(t1.getValue()));
    }
    @Test
    public void testGetLocationOfTemperature(){
        assertEquals("Should have the location of temperature measured","Bedroom", t1.getLocation());
    }
    @Test
    public void testToString(){
        assertEquals("Should print information of object","TemperatureSensor{value=23,location='Bedroom'}", t1.toString() );
    }

}
