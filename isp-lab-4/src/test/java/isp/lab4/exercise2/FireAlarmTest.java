package isp.lab4.exercise2;

import junit.framework.TestCase;
import org.junit.Test;

public class FireAlarmTest extends TestCase {
    FireAlarm fire2=new FireAlarm(false);
    @Test
    public void testIsActiveAlarm(){
        assertEquals("should have false ", false, fire2.isActive());

    }
    @Test
    public void testSetActive(){
        fire2.setActive();
        assertEquals("shoud have true", true,fire2.isActive() );
    }
    @Test
    public void testToString(){
        assertEquals("should have a print", "FireAlarm{active=false}", fire2.toString() );
    }

}