package isp.lab4.exercise6;

import junit.framework.TestCase;

import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Exercise6Test {
    SalariedEmployee salariedEmployee;
    HourlyEmployee hourlyEmployee;
    ComissionEmployee comissionEmployee;

    @Before
    public void init(){
        salariedEmployee = new SalariedEmployee("Doru", "Pop", 123.25);
        hourlyEmployee = new HourlyEmployee("Clau", "Bitman", 10.5, 32.3);
        comissionEmployee = new ComissionEmployee("Andrei", "Asavei", 3500.98, 0.5);
    }

    @Test
    public void testPaymentOfSalariedEmployee() {
        assertEquals("Payment for this employee should be 123.25", "123.25", valueOf(salariedEmployee.getPaymentAmount()));
    }

    @Test
    public void testFirstNameAndLastNameOfEmployee() {
        assertEquals("The name of my employee is Doru Pop ", "DoruPop",salariedEmployee.firstName+salariedEmployee.lastName );
    }
    @Test
    public void testPaymentOfHourlyEmployee() {
        assertEquals("Payment for this employee should be 339.15", "339.15", valueOf(hourlyEmployee.getPaymentAmount()));
    }
    @Test
    public void testPaymentOfComissionEmployee() {
        assertEquals("Payment for this employee should be 1750.49", 1750.49, comissionEmployee.getPaymentAmount(),2);
    }
    @Test
    public void testTotalPayment(){
        Employee[] employees = new Employee[6];
        employees[0] = salariedEmployee;
        employees[1] = new SalariedEmployee("Octavian", "Dumitru", 89.75);
        employees[2] = hourlyEmployee;
        employees[3] = new HourlyEmployee("Dan", "Cizmaru", 9.7, 48);
        employees[4] = comissionEmployee;
        employees[5] = new ComissionEmployee("Ion", "Romascanu", 1890.31, 0.4);
        double payment = 0.0;
        for (Employee employee : employees)
            payment += employee.getPaymentAmount();
        assertEquals("Payment for this employee should be 3524.3639999999996", "3524.3639999999996", valueOf(payment));
    }

}