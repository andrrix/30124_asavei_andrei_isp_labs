package isp.lab4.exercise4;

import isp.lab4.exercise3.FireAlarm;
import isp.lab4.exercise3.TemperatureSensor;
import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Exercise4Test {
    TemperatureSensor[] temperatureSensors=new TemperatureSensor[]{new TemperatureSensor(70, "bedroom"),
            new TemperatureSensor(55, "kitchen"), new TemperatureSensor(60, "bathroom")};
    FireAlarm fireAlarm=new FireAlarm(false);
    Controler controler=new Controler(temperatureSensors,fireAlarm);

    @Test
    public void testControlStep(){
        controler.controlStep();
        assertEquals("The status at FireAlarm 'fireAlarm' should be 'active=true'", true, controler.fireSensor.isActive());
    }
}