package isp.lab4.exercise3;


import junit.framework.TestCase;
import org.junit.Test;

import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;

public class Exercise3Test extends TestCase {
    FireAlarm fire2=new FireAlarm(false);
    TemperatureSensor t1=new TemperatureSensor(23,"Bedroom");
    TemperatureSensor t2=new TemperatureSensor(60, "Kitchen");
    Controler controler=new Controler(t2, fire2);
    @Test
    public void testIsActiveAlarm(){
        assertEquals("should have false ", false, fire2.isActive());

    }
    @Test
    public void testSetActive(){
        fire2.setActive();
        assertEquals("shoud have true", true,fire2.isActive() );
    }
    @Test
    public void testToStringFireAlarm(){
        assertEquals("should have a print", "FireAlarm{active=false}", fire2.toString() );
    }

    @Test
    public void testGetValueOfTemperature() {
        assertEquals("Should have value of temperature", 23, t1.getValue());
    }
    @Test
    public void testGetLocationOfTemperature(){
        assertEquals("Should have the location of temperature measured","Bedroom", t1.getLocation());
    }
    @Test
    public void testToStringTemperatureSensor(){
        assertEquals("Should print information of object","TemperatureSensor{value=23,location='Bedroom'}", t1.toString() );
    }
    @Test
    public void testControlStep(){
        controler.controlStep();
        assertEquals("Should have active", true, controler.fireAlarm.isActive());
    }


}