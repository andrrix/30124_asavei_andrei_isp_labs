package isp.lab4.exercise5;

import isp.lab4.exercise3.FireAlarm;
import isp.lab4.exercise3.TemperatureSensor;

public class Exercise5 {
    public static void main(String[] args) {
        TemperatureSensor[] temperatureSensors=new TemperatureSensor[]{new TemperatureSensor(25, "bedroom"),
                new TemperatureSensor(55, "kitchen"), new TemperatureSensor(18, "bathroom")};
        FireAlarm fireAlarm=new FireAlarm(false);
        Controler controler=new Controler(temperatureSensors,fireAlarm);
        House house=new House(controler);
        house.controler.controlStep();
    }
}
