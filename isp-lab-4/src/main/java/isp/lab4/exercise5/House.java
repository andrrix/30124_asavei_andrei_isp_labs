package isp.lab4.exercise5;


public class House {
    Controler controler;

    public House(Controler controler) {
        this.controler = controler;
    }

    public Controler getControler() {
        return controler;
    }
}
