package isp.lab4.exercise2;

public class Exercise2 {
    public static void main(String[] args) {
        //my code here
        FireAlarm fire1=new FireAlarm(false);
        System.out.println("The initial FireAlarm fire1 is "+fire1.isActive());
        fire1.setActive();
        System.out.println("The final status of FireAlarm fire1 is "+fire1.isActive() );
        System.out.println(fire1);
    }
}
