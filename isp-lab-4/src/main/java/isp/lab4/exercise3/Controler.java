package isp.lab4.exercise3;

public class Controler {
    TemperatureSensor temperatureSensor;
    FireAlarm fireAlarm;

    public Controler(TemperatureSensor temperatureSensor, FireAlarm fireAlarm) {
        this.temperatureSensor = temperatureSensor;
        this.fireAlarm = fireAlarm;
    }
    public void controlStep(){
        if(this.temperatureSensor.getValue()>50)
        {
            this.fireAlarm.setActive();
            System.out.println("Fire alarm started");
        }
        else System.out.println("Fire alarm not started");
    }
}
