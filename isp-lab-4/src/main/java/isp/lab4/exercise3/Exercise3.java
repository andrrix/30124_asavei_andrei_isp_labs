package isp.lab4.exercise3;




public class Exercise3 {
    public static void main(String[] args) {
        //my code here yay
        //instantiate an test TemperatureSensor class
        TemperatureSensor t1=new TemperatureSensor(60, "Bedroom");
        System.out.println("Temperatura in "+ t1.getLocation() +" este de "+t1.getValue() +" grade");
        System.out.println(t1);
        System.out.println();
        //instantiate an test FireAlarm class
        FireAlarm fire1=new FireAlarm(false);
        System.out.println("The initial statud of FireAlarm fire1 is '"+fire1.isActive()+"'");
        fire1.setActive();
        System.out.println("The final status of FireAlarm fire1 is '"+fire1.isActive()+"'" );
        System.out.println(fire1);
        System.out.println();
        //instantiate an test Controler class
        Controler controler=new Controler(t1, fire1);
        controler.controlStep();

    }
}
