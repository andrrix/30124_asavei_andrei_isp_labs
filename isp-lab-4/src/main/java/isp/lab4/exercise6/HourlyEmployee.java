package isp.lab4.exercise6;

public class HourlyEmployee extends Employee{
    double wage;
    double hours;

    public HourlyEmployee(String firstName, String lastName, double wage, double hours) {
        super(firstName, lastName);
        this.wage = wage;
        this.hours = hours;
    }

    public double getPaymentAmount(){
        return this.wage*this.hours;
    };
}
