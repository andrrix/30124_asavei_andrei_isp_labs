package isp.lab4.exercise6;

public class Employee {
    String firstName;
    String lastName;

    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public double getPaymentAmount(){
        return 0.0;
    }
}
