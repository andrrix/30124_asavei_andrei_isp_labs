package isp.lab4.exercise6;

public class SalariedEmployee extends Employee{
    double weeklySalary;

    public SalariedEmployee(String firstName, String lastName, double weeklySalary) {
        super(firstName, lastName);
        this.weeklySalary = weeklySalary;
    }

    public double getPaymentAmount(){
        return this.weeklySalary;
    }
}
