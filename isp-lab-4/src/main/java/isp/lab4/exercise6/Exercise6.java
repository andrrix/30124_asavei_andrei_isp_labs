package isp.lab4.exercise6;

public class Exercise6 {
    public static void main(String[] args) {
        SalariedEmployee salariedEmployee = new SalariedEmployee("Doru", "Pop", 123.25);
        System.out.println(salariedEmployee.getPaymentAmount());
        HourlyEmployee hourlyEmployee = new HourlyEmployee("Clau", "Bitman", 10.5, 32.3);
        System.out.println(hourlyEmployee.getPaymentAmount());
        ComissionEmployee comissionEmployee = new ComissionEmployee("Andrei", "Asavei", 3500.98, 0.5);
        System.out.println(comissionEmployee.getPaymentAmount());
        Employee[] employees = new Employee[6];
        employees[0] = salariedEmployee;
        employees[1] = new SalariedEmployee("Octavian", "Dumitru", 89.75);
        //double d = ((SalariedEmployee) employees[1]).weeklySalary;
        employees[2] = hourlyEmployee;
        employees[3] = new HourlyEmployee("Dan", "Cizmaru", 9.7, 48);
        employees[4] = comissionEmployee;
        employees[5] = new ComissionEmployee("Ion", "Romascanu", 1890.31, 0.4);
        double payment = 0.0;
        for (Employee employee : employees) {
            payment += employee.getPaymentAmount();
        }
        System.out.println("The total Payment is " + payment);
    }
}
