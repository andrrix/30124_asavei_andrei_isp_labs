package isp.lab4.exercise4;

import isp.lab4.exercise3.FireAlarm;
import isp.lab4.exercise3.TemperatureSensor;

public class Exercise4 {
    public static void main(String[] args) {
        TemperatureSensor[] temperatureSensors=new TemperatureSensor[]{new TemperatureSensor(70, "bedroom"),
        new TemperatureSensor(55, "kitchen"), new TemperatureSensor(60, "bathroom")};
        FireAlarm fireAlarm=new FireAlarm(false);
        Controler controler=new Controler(temperatureSensors,fireAlarm);
        controler.controlStep();

    }
}
