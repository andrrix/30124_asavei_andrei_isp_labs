package isp.lab4.exercise4;

import isp.lab4.exercise3.FireAlarm;
import isp.lab4.exercise3.TemperatureSensor;

public class Controler {
    TemperatureSensor[] temperatureSensors = new TemperatureSensor[3];
    FireAlarm fireSensor;

    public Controler(TemperatureSensor[] temperatureSensors, FireAlarm fireSensor) {
        this.temperatureSensors = temperatureSensors;
        this.fireSensor = fireSensor;
    }

    public void controlStep() {
        boolean validate = true;
        for (TemperatureSensor temp : this.temperatureSensors)
            if (temp.getValue() <= 50) {
                validate = false;
                break;
            }
        if (validate) {
            this.fireSensor.setActive();
            System.out.println("Fire alarm started");
        }
        else {

            System.out.println("Fire alarm not started");
        }
    }

}
