package isp.lab2;

import javax.swing.*;
import java.util.Scanner;

public class Exercise6 {

    /**
     * This method should generate the required vector non-recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int[] generateVector(int n) {
        int[] vect=new int[n];
        vect[0]=1;
        vect[1]=2;
        for(int i=2;i<n;i++)
            vect[i]=vect[i-1]*vect[i-2];
        return vect;
        //throw new UnsupportedOperationException();
    }

    /**
     * This method should generate the required vector recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int[] v;
    public static int[] generateRandomVectorRecursively(int n) {
        if (v == null) {
            v = new int[n];
            v[0] = 1;
            v[1] = 2;
        }

        if (n > 2) {
            generateRandomVectorRecursively(n - 1);
            v[n - 1] = v[n - 2] * v[n - 3];
        }

        return v;
        //throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.println("Give me a number 'n':");
        int n=s.nextInt();
        while (n<8) {
                System.out.println("Sorry wrong number! It must be >= 8, Give me another:");
                n=s.nextInt();
            }
        // TODO: print the vectors
        int[] vect=generateVector(n);
        System.out.println("A normal type: ");
        for(int vec:vect)
            System.out.print(vec +", ");
        vect=generateRandomVectorRecursively(n);
        System.out.println();
        System.out.println("A recursive type: ");
        for(int vec:vect)
            System.out.print(vec +", ");
    }
}
