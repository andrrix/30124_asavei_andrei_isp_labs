package isp.lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {

    /**
     * This method should generate a random number which
     * represent the position in the given arrays,so
     * the random should be between 0 and 7
     *
     * @return the generated random
     */
    public static int generateARandom() {
        int min=0,max=5;
        int n;
        Random r=new Random();
        n=r.nextInt(max-min)+min;
        return n;
        //throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
//        int[] ucl = new int[]{1, 2, 3, 4, 5, 6, 7, 13};
//        int[] answers = new int[]{10, 4, 2, 1, 2, 1, 1, 1};
        int times = generateARandom();
        int counter=1,m = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Guess the number! : ");
        while(true) {
            System.out.println("Give your number:");
            int number = scan.nextInt();
            if (number == times) {
                System.out.println("You guessed the number!" + '\n' + "Number of tries:" + counter);
                break;
            }
             else if (number > times)
                System.out.println("Your number is too big!");
             else System.out.println("Your number is too small!");
               counter++;
        }

    }
}
