package isp.lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise1 {

    /**
     * This method should generate an random number between 2 and 10 then
     * should ask user to enter that amount of numbers from keyboard and
     * store the numbers in an int array which should be returned
     *
     * @return the array of numbers read from keyboard
     */
    private static int[] getUserNumbers() {
        Random r=new Random();
        int n=r.nextInt(10-2)+3;
        System.out.println(n);
        int[] a=new int[n];
        Scanner s=new Scanner(System.in);
        System.out.println("Give " + n + " numbers: ");
        for(int i=0;i<a.length;i++){
            a[i]=s.nextInt();}
        return a;
        //throw new UnsupportedOperationException();
    }

    /**
     * This method should compute the arithmetical mean of the given numbers
     *
     * @param userNumbers the numbers used to calculate the arithmetical mean
     * @return the arithmetical mean of the given numbers
     */
    protected static double computeTheArithmeticalMean(int[] userNumbers) {
        int sum=0;
        for(int i=0;i<userNumbers.length;i++)
            sum=sum+userNumbers[i];
        return (double) sum/userNumbers.length;
        //throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
        int[] userNumbers = getUserNumbers();
        System.out.println("Mean number is: " + computeTheArithmeticalMean(userNumbers));
    }


}
