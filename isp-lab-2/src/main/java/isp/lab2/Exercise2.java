package isp.lab2;

import java.sql.SQLOutput;
import java.util.Scanner;

/**
 * Check readme.md file for the exercise requirements.
 *
 * @author Radu Miron
 */
public class Exercise2 {
    /**
     * Reads an enumeration of words separated by comma.
     *
     * @return The console input enumeration if it's valid, null otherwise.
     */
    public static String readEnumeration() {
        System.out.println("Please input an enumeration of words separated by comma:");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Give word, on one line, separated by ',' :");
        String enumeration = scanner.nextLine();
        System.out.println(enumeration);
        if (enumeration.trim().length() > 0) {
            return enumeration;
        } else {
            return null;
        }
    }

    /**
     * Reads the searched words starting letter.
     *
     * @return The starting character if it's a valid letter, null otherwise.
     */
    public static Character readStartingLetter() {
        System.out.println("Please input the starting letter of the words you want to search:");

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        if (line.length() > 0) {
            return line.charAt(0);
        } else {
            return null;
        }
    }

    /**
     * Splits a given {@link String} by comma.
     * TODO: Use {@link String#split(String splitBy)} to split the enumeration;
     * TODO: If you plan on using also whitespace characters in your enumeration (eg. " apple , cat, mouse"), use {@link String#trim()} for each word.
     *
     * @param inputEnumeration The words' enumeration.
     * @return An array of words.
     */
    public static String[] splitEnumerationIntoWords(String inputEnumeration) {
        String[] parts = inputEnumeration.split(",");
        for(int i=0;i<parts.length;i++)
            parts[i]=parts[i].trim();

        return parts;
       // throw new UnsupportedOperationException("Not implemented yet!");
    }

    /**
     * Finds the words starting with a given letter in an array.
     *
     * @param words          The array of words.
     * @param startingLetter The starting letter.
     * @return All the words starting with the given letter.
     */
    public static String[] findWordsStartingWith(String[] words, Character startingLetter) {
        String[] s=new String[words.length];
        int j=0;
        for(int i=0;i<words.length;i++)
            if(startingLetter==words[i].charAt(0)) {
               Character c= words[i].charAt(0);
                s[j]=words[i];
                j++;
            }
        String[] nou=new String[j];
            for(int p=0;p<j;p++)
                nou[p]=s[p];
            return nou;
        //throw new UnsupportedOperationException("Not implemented yet!");
    }

    public static void main(String[] args) {
        String enumeration = readEnumeration();
        Character startingLetter = readStartingLetter();

        if (enumeration != null && startingLetter != null) {
            String[] words = splitEnumerationIntoWords(enumeration);
            String[] results = findWordsStartingWith(words, startingLetter);
          for(String r:results)
              if(r!=null)
            System.out.println(r);
        } else {
            System.out.println("Doesn't exist a word that starts with "+ startingLetter);
        }

    }
}
